import request from '@/utils/request'

export function login (data) {
  return request({
    url: '/waplogin.aspx',
    method: 'post',
    data
  })
}

export function getUserInfo () {
  return request({
    url: '/myfile.aspx',
    method: 'get'
  })
}

// 创建
export function postGame (data) {
  return request({
    url: '/games/chuiniu/add.aspx',
    method: 'post',
    data
  })
}

// 获取发起列表
export function getMyGameList (params) {
  return request({
    url: '/games/chuiniu/book_list.aspx',
    method: 'get',
    params
  })
}
