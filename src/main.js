import { createApp } from 'vue'
import App from './App.vue'
import Vant from 'vant';
import './style/index.css';
import 'vant/lib/index.css';

createApp(App).use(Vant).mount('#app')
